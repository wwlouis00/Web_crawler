import pygal .maps.world

worldMap = pygal.maps.world.World()
worldMap.title = "Country in the Map"   #建立世界地圖物件
worldMap.add('China',['cn'])            #世界地圖標題
worldMap.add('Singapore',['sg'])        #標記國家
worldMap.add('Canada',['ca'])           #儲存地圖檔案
worldMap.add('Iraq',['iq'])
worldMap.render_to_file("Country in the world.svg")
